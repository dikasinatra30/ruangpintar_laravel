<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMateriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_category');
            $table->string('kd_materi');
            $table->string('image_path');
            $table->string('nama_materi');
            $table->string('deskripsi');
            $table->integer('jumlah_vidio');
            $table->unsignedbigInteger('menit');
            $table->double('harga');
            $table->string('link_youtube');
            $table->timestamps();

            $table->foreign('id_category')
                    ->references('id')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materi');
    }
}
