<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Route::get('/', 'HomeController@index')->name('home.index');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::group(['prefix' => 'materi', ], function () {
    Route::get('detail-materi/{materi}', 'MateriController@show')->name('materi.show');
    Route::get('checkout/{materi}', 'MateriController@checkout')->name('materi.checkout');
    Route::get('transaksi/{transaksi}', 'MateriController@transaksi')->name('transaksi.show');
    // Route::post('customers/{customer}/status', 'CustomerController@update_active')->name('customers.update_active');
});

Route::group(['prefix' => 'kelas', ], function () {
    Route::get('kelasku', 'KelasController@show')->name('kelas.show');
});

Route::group(['prefix' => 'admin','namespace' => 'Admin','middleware' => ['auth']], function () {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    Route::group(['prefix' => 'kelas'], function () {
        Route::get('kelas', 'KelasController@index')->name('kelas.index');
        Route::get('kelas/{kelas}', 'KelasController@update')->name('kelas.update');
    });

    Route::group(['prefix' => 'murid'], function () {
        Route::get('murid', 'MuridController@index')->name('murid.index');
        Route::get('murid/{murid}', 'MuridController@update')->name('murid.update');
    });

    Route::group(['prefix' => 'transaksi'], function () {
        Route::get('transaksi', 'TransaksiController@index')->name('transaksi.index');
        Route::get('transaksi/print', 'TransaksiController@print')->name('transaksi.print');
    });

    Route::group(['prefix' => 'category'], function () {
        Route::get('category', 'CategoryController@index')->name('category.index');
        Route::get('tambah-category', 'CategoryController@form_tambah')->name('category.form_tambah');
        Route::post('category/store', 'CategoryController@store')->name('category.store');
        Route::get('update-category/{category}', 'CategoryController@form_update')->name('category.form_update');
        Route::post('update/{category}', 'CategoryController@update')->name('category.update');
        Route::get('hapus/{category}', 'CategoryController@destroy')->name('category.destroy');
    });

    Route::group(['prefix' => 'materi'], function () {
        Route::get('materi', 'MateriController@index')->name('materi.index');
        Route::get('tambah-materi', 'MateriController@form_tambah')->name('materi.form_tambah');
        Route::post('materi/store', 'MateriController@store')->name('materi.store');
        Route::get('update-materi/{materi}', 'MateriController@form_update')->name('materi.form_update');
        Route::post('update/{materi}', 'MateriController@update')->name('materi.update');
        Route::get('hapus/{materi}', 'MateriController@destroy')->name('materi.destroy');
    });
});
// Route::get('/home', 'HomeController@index')->name('home');
