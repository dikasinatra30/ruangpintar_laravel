<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <title>Transaksi</title>
    </head>
    <body>
        <div class="container">
            <div style="margin-top: 180px">
                <div class="text-center">
                    <h4>Form Checkout</h4>
                </div>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Kode Trans</th>
                            <td>:</td>
                            <td>{{ $transaksi->kd_trans }}</td>
                        </tr>
                        <tr>
                            <th>Kode Kelas</th>
                            <td>:</td>
                            <td>{{ $transaksi->kd_kelas }}</td>
                        </tr>
                        <tr>
                            <th>Username</th>
                            <td>:</td>
                            <td>{{ $transaksi->username }}</td>
                        </tr>
                        <tr>
                            <th>Harga</th>
                            <td>:</td>
                            <td>Rp {{ number_format($transaksi->harga, 0, ',', '.') }}</td>
                        </tr>
                        <tr class="text-center">
                            <td colspan="3"><a href="https://api.whatsapp.com/send?phone=+6288230849902&text=Halo%20Saya%20{{$transaksi->username}}%20Ingin%20Membeli%20Kelas%20:%0A{{$materi}}" class="btn btn-primary" type="submit">Check Out</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </body>
</html>
