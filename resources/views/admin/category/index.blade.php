@extends('layout_admin.app')

@section('content')
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Category</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"><a href="#">Category</a></li>
                    </ol>
                </div>
                <div class="col-md-12 text-right">
                    <a href="{{route('category.form_tambah')}}" class="btn btn-success">+ Tambah Category</a>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 bg-white pt-4 pb-4">
                    <table class="table">
                        <thead class="thead-light">
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Aktif</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($categorys as $category)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$category->nama}}</td>
                            <td>{{$category->aktif}}</td>
                            <td>
                                <a href="{{ route('category.form_update', $category->id) }}" type="submit" class="btn btn-warning">Update</a>
                                <a href="{{ route('category.destroy', $category->id) }}" type="submit" class="btn btn-danger">Hapus</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
