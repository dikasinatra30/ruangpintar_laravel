@extends('layout_admin.app')

@section('content')
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Tambah Category</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Category</a></li>
                        <li class="breadcrumb-item active"><a href="#">Tambah Category</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 bg-white pt-4 pb-4">
                    <form method="POST" action="{{ route('category.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="nama">Nama Category</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Input Kode Materi" required>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="aktif" value="ya" id="defaultCheck1">
                            <label class="form-check-label" for="defaultCheck1">
                              Aktif
                            </label>
                        </div>
                        <div class="form-grout">
                            <button type="submit" class="btn btn-info">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
