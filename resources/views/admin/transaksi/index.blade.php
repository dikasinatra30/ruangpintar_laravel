@extends('layout_admin.app')

@section('content')
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Transaksi</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"><a href="#">Transaksi</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-right mb-3">
                    <a href="{{ route('transaksi.print') }}" class="btn-success py-1 px-5 rounded">Print PDF</a>
                </div>
                <div class="col-md-12 bg-white pt-4 pb-4">
                    <table class="table">
                        <thead class="thead-light">
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Kode Transaksi</th>
                            <th scope="col">Kode Kelas</th>
                            <th scope="col">Username</th>
                            <th scope="col">Harga</th>
                            <th scope="col">Jadi Beli</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($transaksis as $transaksi)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$transaksi->kd_trans}}</td>
                            <td>{{$transaksi->kd_kelas}}</td>
                            <td>{{$transaksi->username}}</td>
                            <td>Rp {{number_format($transaksi->harga, 0, ',', '.')}}</td>
                            <td>
                                @if($transaksi->jadi_beli == 1)
                                    Ya
                                @else
                                    Belum
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
