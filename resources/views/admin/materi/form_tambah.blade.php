@extends('layout_admin.app')

@section('content')
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Materi</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Materi</a></li>
                        <li class="breadcrumb-item active"><a href="#">Tambah Materi</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 bg-white pt-4 pb-4">
                    <form method="POST" action="{{ route('materi.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="disabledSelect">Category</label>
                            <select id="disabledSelect" name="id_category" class="form-control" required>
                                <option>Pilih Category</option>
                                <option value="1">PHP</option>
                                <option value="2">CSS</option>
                                <option value="3">HTML</option>
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="kd_materi">Kode Materi</label>
                          <input type="text" class="form-control" id="kd_materi" name="kd_materi" placeholder="Input Kode Materi" required>
                        </div>
                        <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file" class="form-control-file" name="image_path" id="image" required>
                        </div>
                        <div class="form-group">
                            <label for="nama_materi">Nama Materi</label>
                            <input type="text" class="form-control" id="nama_materi" name="nama_materi" placeholder="Input Nama Materi" required>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi</label>
                            <textarea type="text" class="form-control" id="deskripsi" name="deskripsi" placeholder="Input Deskripsi" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="jumlah_vidio">Jumlah Vidio</label>
                            <input type="number" class="form-control" id="jumlah_vidio" name="jumlah_vidio" placeholder="Input Jumlah Vidio" required>
                        </div>
                        <div class="form-group">
                            <label for="menit">Menit</label>
                            <input type="number" class="form-control" id="menit" name="menit" placeholder="Input Menit" required>
                        </div>
                        <div class="form-group">
                            <label for="harga">Harga</label>
                            <input type="text" class="form-control" id="harga" name="harga" placeholder="Input Harga" required>
                        </div>
                        <div class="form-grout">
                            <button type="submit" class="btn btn-info">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
