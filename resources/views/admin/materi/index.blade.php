@extends('layout_admin.app')

@section('content')
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Materi</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"><a href="#">Materi</a></li>
                    </ol>
                </div>
                <div class="col-md-12 text-right">
                    <a href="{{route('materi.form_tambah')}}" class="btn btn-success">+ Tambah Materi</a>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 bg-white pt-4 pb-4">
                    <table class="table">
                        <thead class="thead-light">
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Category</th>
                            <th scope="col">Kode Materi</th>
                            <th scope="col">Nama Materi</th>
                            <th scope="col">Deskripsi</th>
                            <th scope="col">Jumlah Vidio</th>
                            <th scope="col">Menit</th>
                            <th scope="col">Harga</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($materis as $materi)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$materi->category->nama}}</td>
                            <td>{{$materi->kd_materi}}</td>
                            <td>{{$materi->nama_materi}}</td>
                            <td>{{$materi->deskripsi}}</td>
                            <td>{{$materi->jumlah_vidio}}</td>
                            <td>{{$materi->menit}}</td>
                            <td>Rp {{number_format($materi->harga, 0, ',', '.')}}</td>
                            <td>
                                <a href="{{ route('materi.form_update', $materi->id) }}" type="submit" class="btn btn-warning">Update</a>
                                <a href="{{ route('materi.destroy', $materi->id) }}" type="submit" class="btn btn-danger">Hapus</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
