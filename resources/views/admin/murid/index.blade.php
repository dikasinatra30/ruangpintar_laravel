@extends('layout_admin.app')

@section('content')
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Murid</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"><a href="#">Murid</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 bg-white pt-4 pb-4">
                    <table class="table">
                        <thead class="thead-light">
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Username</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">Permission</th>
                            <th scope="col">Email</th>
                            <th scope="col">No Telp</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($murids as $murid)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$murid->username}}</td>
                            <td>{{$murid->nama}}</td>
                            <td>{{$murid->alamat}}</td>
                            <td>
                                @if($murid->permission == 1)
                                    Admin
                                @else
                                    User
                                @endif
                            </td>
                            <td>{{$murid->email}}</td>
                            <td>{{$murid->no_telp}}</td>
                            <td><a href="{{ route('murid.update', $murid->id) }}" type="submit" class="btn btn-warning">Update</a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
