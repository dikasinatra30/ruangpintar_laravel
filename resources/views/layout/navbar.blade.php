<header>
    <div class="header-area ">
        <div id="sticky-header" class="main-header-area">
            <div class="container-fluid p-0">
                <div class="row align-items-center no-gutters">
                    <div class="col-xl-2 col-lg-2">
                        <div class="logo-img">
                            <a href="{{ route('home.index') }}">
                                <img src="img/logo.png" alt="No Image">
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7">
                        <div class="main-menu  d-none d-lg-block">
                            <nav>
                                <ul id="navigation">
                                    <li><a class="{{ Request::routeIs('home.index') ? 'active' : '' }}" href="{{route('home.index')}}">home</a></li>
                                    <li><a class="{{ Request::routeIs('kelas.show') ? 'active' : '' }}" href="{{route('kelas.show')}}">Kelasku</a></li>
                                    @if(Auth::check())
                                        @if(Auth::user()->permission == 1)
                                        <li><a href="{{ route('dashboard') }}">Kelola Kelas</a></li>
                                        @endif
                                    @endif
                                    {{-- <li><a class="{{ route('kelas.show') ? 'active' : '' }}" href="contact.php">Contact</a></li> --}}
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                    @if (Auth::check())
                        <div class="log_chat_area d-flex align-items-center">
                            <a href="#test-form3" class="login popup-with-form">
                                <i class="flaticon-user"></i>
                                <span>{{ Auth::user()->nama }}</span>
                            </a>
                    @else
                        <div class="log_chat_area d-flex align-items-center">
                            <a href="#test-form" class="login popup-with-form">
                                <i class="flaticon-user"></i>
                                <span>Log In</span>
                            </a>
                    @endif
                            <div class="live_chat_btn">
                                <a class="boxed_btn_orange" href="#">
                                    <i class="fa fa-phone"></i>
                                    <span>+62 852 3044 2269</span>
                                </a>
                            </div>
                        </div>
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
