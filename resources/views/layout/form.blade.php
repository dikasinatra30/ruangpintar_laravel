<form id="test-form" action="{{ route('login') }}" method="post" class="white-popup-block mfp-hide">
    @csrf
    <div class="popup_box ">
        <div class="popup_inner">
            <div class="logo text-center">
                <a href="">
                    <img src="img/form-logo.png" alt="">
                </a>
            </div>
            <h3>Sign in</h3>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('login') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <input class="form-control @error('email') is-invalid @enderror" id="email" type="email" name="email" value="{{ old('email') }}" placeholder="Email" autocomplete="email" required>
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input class="form-control @error('password') is-invalid @enderror" id="password" type="password" name="password" placeholder="Password" autocomplete="current-password" required>
                    </div>
                    <div class="col-xl-12">
                        <button type="submit" class="boxed_btn_orange">{{ __('Login') }}</button>
                    </div>
                </div>
            </form>
            <p class="doen_have_acc">Don’t have an account? <a class="dont-hav-acc" href="#test-form2">Sign Up</a>
            </p>
        </div>
    </div>
</form>
<form id="test-form2" action="{{ route('register') }}" method="POST" class="white-popup-block mfp-hide">
    @csrf
    <div class="popup_box ">
        <div class="popup_inner">
            <div class="logo text-center">
                <a href="#">
                    <img src="img/form-logo.png" alt="">
                </a>
            </div>
            <h3>Registrasi</h3>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('register') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <input type="text" name="username" placeholder="Username" required>
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="password" name="password" placeholder="Password" required>
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="text" name="nama" placeholder="Nama" required>
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="text" name="alamat" placeholder="Alamat" required>
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="email" name="email" placeholder="Enter email" required>
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="text" name="notelp" placeholder="No Telpon" required>
                    </div>
                    <div class="col-xl-12">
                        <button type="submit" class="boxed_btn_orange">Sign Up</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</form>
<form id="test-form3" action="{{ route('logout') }}" method="get" class="white-popup-block mfp-hide">
    <div class="popup_box ">
        <div class="popup_inner">
            <div class="logo text-center">
                <a href="#">
                    <img src="img/form-logo.png" alt="">
                </a>
            </div>
            <h3 class="mb-3">Halo {{ Auth::user() ? Auth::user()->nama : '' }}</h3>
            <h3>Selamat Belajar !</h3>
            <form action="">
                <div class="row">
                    {{-- <div class="col-xl-12 col-md-12">
                       <h3></h3>
                    </div> --}}
                    <div class="col-xl-12">
                        <button type="submit" name="submit" value="keluar" class="boxed_btn_orange">Keluar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</form>
<form id="test-form4" method="post" class="white-popup-block mfp-hide">
    <div class="popup_box ">
        <div class="popup_inner">
            <div class="logo text-center">
                <a href="#">
                    <img src="img/form-logo.png" alt="">
                </a>
            </div>
            <h3>Registrasi</h3>
            <form action="">
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <input type="text" name="username" placeholder="Username">
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="password" name="pw" placeholder="Password">
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="Password" name="pw2" placeholder="Confirm password">
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="text" name="nama" placeholder="Nama">
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="text" name="alamat" placeholder="Alamat">
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="email" name="email" placeholder="Enter email">
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="text" name="telp" placeholder="No Telpon">
                    </div>
                    <div class="col-xl-12">
                        <button type="submit" name="submit" value="daftar" class="boxed_btn_orange">Sign Up</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</form>
