<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cohensive\Embed\Facades\Embed;

class Materi extends Model
{
    protected $table = 'materi';
    protected $visible = ['id'];

    protected $fillable = [
        'id_category', 'kd_materi', 'image_path', 'nama_materi', 'deskripsi', 'jumlah_vidio', 'menit' , 'harga'
    ];

    public function category()
    {
      return $this->belongsTo('App\Models\Category', 'id_category');
    }

    // public function getVideoHtmlAttribute()
    // {
    //     $embed = Embed::make($this->video)->parseUrl();

    //     if (!$embed)
    //         return '';

    //     $embed->setAttribute(['width' => 400]);
    //     return $embed->getHtml();
    // }
}


