<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $visible = ['id'];

    protected $fillable = [
        'nama', 'image_path', 'aktif'
    ];

    // public function materi()
    // {
    //   return $this->hasMany('App\Models\Materi', 'id_category');
    // }
}
