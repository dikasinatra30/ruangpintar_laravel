<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Murid;
use Auth;

class MuridController extends Controller
{
    public function index(){
        $murids = Murid::All();
        return view('admin/murid/index')
            ->with('murids', $murids);
    }

    public function update(Murid $murid){
        if($murid->permission == 0){
            $murid->permission = 1;
            $murid->save();

            return redirect()->route('murid.index');
        }

        if($murid->permission == 1){
            $murid->permission = 0;
            $murid->save();

            return redirect()->route('murid.index');
        }

        return redirect()->route('murid.index');
    }
}
