<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Auth;

class CategoryController extends Controller
{
    public function index(){
        $categorys = Category::All();
        return view('admin/category/index')
            ->with('categorys', $categorys);
    }

    public function form_tambah(){
        //
        return view('admin/category/form_tambah');
    }

    public function store(Request $request){
        $category = New Category;
        $category->fill($request->all());
        $category->save();

        return redirect()->route('category.index');
    }

    public function form_update(Category $category){
        //
        return view('admin/category/form_update')
            ->with('category', $category);
    }

    public function update(Request $request, Category $category){
        $materi->fill($request->all());
        $materi->save();

        return redirect()->route('category.index');
    }

    public function destroy(Category $category){
        $category->delete();
        return redirect()->route('category.index');
    }
}
