<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Materi;
use Auth;

class MateriController extends Controller
{
    public function index(){
        $materis = Materi::All();
        return view('admin/materi/index')
            ->with('materis', $materis);
    }

    public function form_tambah(){
        //
        return view('admin/materi/form_tambah');
    }

    public function store(Request $request){
        $materi = New Materi;
        // dd(request::all());
        $materi->fill($request->all());
        $materi->save();

        return redirect()->route('materi.index');
    }

    public function form_update(Materi $materi){
        //
        return view('admin/materi/form_update')
            ->with('materi', $materi);
    }

    public function update(Request $request, Materi $materi){
        $materi->fill($request->all());
        $materi->save();

        return redirect()->route('materi.index');
    }

    public function destroy(Materi $materi){
        $materi->delete();
        return redirect()->route('materi.index');
    }
}
