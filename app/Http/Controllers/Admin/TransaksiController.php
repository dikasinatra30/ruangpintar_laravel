<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaksi;
use Auth;
use PDF;

class TransaksiController extends Controller
{
    public function index(){
        $transaksis = Transaksi::All();
        return view('admin/transaksi/index')
            ->with('transaksis', $transaksis);
    }

    public function print()
    {
    	$transaksi = Transaksi::all();

        $pdf = PDF::loadview('admin.transaksi.trans_pdf',['transaksi'=>$transaksi]);
        return $pdf->download('laporan-transaksi-pdf');
    }
}
