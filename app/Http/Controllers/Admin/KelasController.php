<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kelas;
use App\Models\Transaksi;
use Auth;

class KelasController extends Controller
{
    public function index(){
        $kelass = Kelas::All();
        return view('admin/kelas/index')
            ->with('kelass', $kelass);
    }

    public function update(Kelas $kelas){

        if(Auth::user()->permission == 0){
            $transaksi = Transaksi::where('username', Auth::user()->username)->first();
            if($kelas->aktif == "Tidak"){
                $kelas->aktif = "Ya";
                $kelas->save();

                $transaksi->jadi_beli = 1;
                $transaksi->save();
                return redirect()->route('kelas.index');
            }

            if($kelas->aktif == "Ya"){
                $kelas->aktif = "Tidak";
                $kelas->save();

                $transaksi->jadi_beli = 0;
                $transaksi->save();
                return redirect()->route('kelas.index');
            }
        }

        if(Auth::user()->permission == 1){
            if($kelas->aktif == "Tidak"){
                $kelas->aktif = "Ya";
                $kelas->save();

                return redirect()->route('kelas.index');
            }

            if($kelas->aktif == "Ya"){
                $kelas->aktif = "Tidak";
                $kelas->save();

                return redirect()->route('kelas.index');
            }
        }


        return redirect()->route('kelas.index');
    }
}
