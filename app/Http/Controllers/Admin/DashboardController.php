<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kelas;
use App\Models\Murid;
use App\Models\Transaksi;
use Auth;

class DashboardController extends Controller
{
    public function index(){
        $kelas = Kelas::All()->count();
        $murid = Murid::All()->count();
        $transaksi = Transaksi::All()->count();
        return view('admin/dashboard')
            ->with('kelas', $kelas)
            ->with('murid', $murid)
            ->with('transaksi', $transaksi);
    }
}
