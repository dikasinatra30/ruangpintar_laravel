<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kelas;
use Auth;

class KelasController extends Controller
{
    public function show(){
        if(!Auth::check()){
            return redirect()->back();
        }

        $cek_kelas = Kelas::where('username', Auth::user()->username)->where('aktif', 'Ya')->first();
        if($cek_kelas){
            if($cek_kelas->kd_materi == "PHPDS"){
                return view('ruangpintar.kelas.php');
            }

            elseif($cek_kelas->kd_materi == "CSSDS"){
                return view('ruangpintar.kelas.css');
            }

            elseif($cek_kelas->kd_materi == "HTMLDS"){
                return view('ruangpintar.kelas.html');
            }
        }
        return view('ruangpintar.kelas.no_kelas');
    }
}
