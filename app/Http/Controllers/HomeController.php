<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Materi;
use App\Models\Murid;
use App\Models\Transaksi;

class HomeController extends Controller
{
    public function index(){
        $materis = Materi::query()->get();
        $murids = Murid::query()->count();
        $transaksis = Transaksi::query()->count();

        return view('ruangpintar.home')
        ->with('materis', $materis)
        ->with('murids', $murids)
        ->with('transaksis', $transaksis);
    }
}
