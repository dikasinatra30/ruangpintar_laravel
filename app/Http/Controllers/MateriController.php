<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Models\Materi;
use App\Models\Transaksi;
use App\Models\Kelas;
use DB;
use Illuminate\Database\QueryException;
use Auth;

class MateriController extends Controller
{
    public function show(Materi $materi){
        return view('ruangpintar.materi.detail_materi')
                ->with('materi', $materi);
    }

    public function checkout(Materi $materi){
        try {
            DB::beginTransaction();

            if(!Auth::check()){
                return redirect()->back();
            }

            $trans_user = Transaksi::where('username', Auth::user()->username)->first();
            if($trans_user){
                return redirect()->route('kelas.show');
            }

            $transaksi = new Transaksi;
            $transaksi->kd_trans = date("d/m/Y").'/'.strtoupper(Auth::user()->username);
            $transaksi->kd_kelas = $materi->kd_materi;
            $transaksi->username = Auth::user()->username;
            $transaksi->harga = $materi->harga;
            $transaksi->save();

            $kelas_user = Kelas::where('username', Auth::user()->username)->first();
            if(!$kelas_user){
                $kelas = new Kelas;
                $kelas->kd_materi = $materi->kd_materi;
                $kelas->username = Auth::user()->username;
                $kelas->aktif = 'Tidak';
                $kelas->save();
            }

            DB::commit();
            // $request->session()->flash('toast', 'transaksi created successfully!');
            return redirect()->route('transaksi.show', $transaksi->id);
        } catch(QueryException $ex){
            DB::rollBack();
            dd($ex);
            // $request->session()->flash('error', 'Failed to create transaksi. ' . substr($ex->getMessage(),0,30));
            return redirect()->back();
        }
    }

    public function transaksi(Transaksi $transaksi){
        $namamateri = Materi::where('kd_materi', $transaksi->kd_kelas)->first();
        $materi = $namamateri->nama_materi;

        return view('ruangpintar.materi.transaksi')
            ->with('transaksi', $transaksi)
            ->with('materi', $materi);
    }
}
